#!/usr/bin/env python
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import os
import logging
import re
import time

from natural.date import delta
from xtesting.core import testcase
from kubernetes import client, config
from kubernetes.stream import stream
from urllib3.exceptions import MaxRetryError, NewConnectionError
from jinja2 import Environment, PackageLoader, select_autoescape

from chart_status.resources import Deployment, StatefulSet, DaemonSet, Pvc, Job

NAMESPACE = os.getenv('K8S_NAMESPACE', 'onap')
RELEASE = os.getenv('RELEASE', 'onap')
CHART = os.getenv('CHART', 'so')


class Status(testcase.TestCase):
    """Retrieve status of Kubernetes resources from a specific chart."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init the testcase."""
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'chart_status'
        super(Status, self).__init__(**kwargs)
        config.load_kube_config()
        self.core = client.CoreV1Api()
        self.batch = client.BatchV1Api()
        self.app = client.AppsV1Api()
        self.extensions = client.ExtensionsV1beta1Api()
        self.res_dir = "{}/chart-status".format(self.dir_results)

        self.__logger.debug("chart status init started")
        self.start_time = None
        self.stop_time = None
        self.result = 0
        self.jobs = []
        self.deployments = []
        self.statefulsets = []
        self.daemonsets = []
        self.pvcs = []
        self.details = {}
        self.regex = re.compile("^{}-{}-".format(RELEASE, CHART))

    def run(self):
        """Run tests."""
        self.start_time = time.time()
        os.makedirs(self.res_dir, exist_ok=True)
        self.__logger.debug("start test")
        try:
            self.k8s_jobs = self.batch.list_namespaced_job(NAMESPACE).items
            self.__logger.info("%4s Jobs in the namespace", len(self.k8s_jobs))

            self.k8s_deployments = self.app.list_namespaced_deployment(
                NAMESPACE).items
            self.__logger.info("%4s Deployments in the namespace",
                               len(self.k8s_deployments))

            self.k8s_statefulsets = self.app.list_namespaced_stateful_set(
                NAMESPACE).items
            self.__logger.info("%4s StatefulSets in the namespace",
                               len(self.k8s_statefulsets))

            self.k8s_daemonsets = self.app.list_namespaced_daemon_set(
                NAMESPACE).items
            self.__logger.info("%4s DaemonSets in the namespace",
                               len(self.k8s_daemonsets))

            self.k8s_pvcs = self.core.list_namespaced_persistent_volume_claim(
                NAMESPACE).items
            self.__logger.info("%4s PVCs in the namespace", len(self.pvcs))
        except (ConnectionRefusedError, MaxRetryError, NewConnectionError):
            self.__logger.error("chart status test failed.")
            self.__logger.error("cannot connect to Kubernetes.")
            return testcase.TestCase.EX_TESTCASE_FAILED

        self.failing_statefulsets = []
        self.failing_jobs = []
        self.failing_deployments = []
        self.failing_daemonsets = []
        self.failing_pvcs = []
        self.failing = False

        self.jinja_env = Environment(autoescape=select_autoescape(['html']),
                                     loader=PackageLoader('chart_status'))
        self.parse_jobs()
        self.parse_deployments()
        self.parse_statefulsets()
        self.parse_daemonsets()
        self.parse_pvcs()

        self.jinja_env.get_template('raw_output.txt.j2').stream(
            ns=self, namespace=NAMESPACE).dump('{}/onap-k8s.log'.format(
                self.res_dir))

        self.stop_time = time.time()
        if len(self.jobs) > 0:
            self.details['jobs'] = {
                'number': len(self.jobs),
                'number_failing': len(self.failing_jobs),
                'failing': self.map_by_name(self.failing_jobs)
            }
        if len(self.deployments) > 0:
            self.details['deployments'] = {
                'number': len(self.deployments),
                'number_failing': len(self.failing_deployments),
                'failing': self.map_by_name(self.failing_deployments)
            }
        if len(self.statefulsets) > 0:
            self.details['statefulsets'] = {
                'number': len(self.statefulsets),
                'number_failing': len(self.failing_statefulsets),
                'failing': self.map_by_name(self.failing_statefulsets)
            }
        if len(self.daemonsets) > 0:
            self.details['daemonsets'] = {
                'number': len(self.daemonsets),
                'number_failing': len(self.failing_daemonsets),
                'failing': self.map_by_name(self.failing_daemonsets)
            }
        if len(self.pvcs) > 0:
            self.details['pvcs'] = {
                'number': len(self.pvcs),
                'number_failing': len(self.failing_pvcs),
                'failing': self.map_by_name(self.failing_pvcs)
            }
        if self.failing:
            self.__logger.error("namespace status test failed.")
            self.__logger.error("number of errored Jobs: %s",
                                len(self.failing_jobs))
            self.__logger.error("number of errored Deployments: %s",
                                len(self.failing_deployments))
            self.__logger.error("number of errored StatefulSets: %s",
                                len(self.failing_statefulsets))
            self.__logger.error("number of errored DaemonSets: %s",
                                len(self.failing_daemonsets))
            self.__logger.error("number of errored PVCs: %s",
                                len(self.failing_pvcs))
            return testcase.TestCase.EX_TESTCASE_FAILED

        self.result = 100
        return testcase.TestCase.EX_OK

    def parse_jobs(self):
        """Parse the jobs."""
        self.__logger.info("%4s jobs to parse", len(self.k8s_jobs))
        for i in range(len(self.k8s_jobs)):
            k8s = self.k8s_jobs[i]
            if self.regex.match(k8s.metadata.name):
                job = Job(k8s=k8s)

                if not k8s.status.completion_time:
                    self.__logger.debug("a Job is in error")
                    self.failing_jobs.append(job)
                    self.failing = True
                self.jobs.append(job)

    def parse_deployments(self):
        """Parse the deployments."""
        self.__logger.info("%4s deployments to parse",
                           len(self.k8s_deployments))
        for i in range(len(self.k8s_deployments)):
            k8s = self.k8s_deployments[i]
            if self.regex.match(k8s.metadata.name):
                deployment = Deployment(k8s=k8s)

                if k8s.status.unavailable_replicas:
                    self.__logger.debug("a Deployment is in error")
                    self.failing_deployments.append(deployment)
                    self.failing = True

                self.deployments.append(deployment)

    def parse_statefulsets(self):
        """Parse the statefulsets."""
        self.__logger.info("%4s statefulsets to parse",
                           len(self.k8s_statefulsets))
        for i in range(len(self.k8s_statefulsets)):
            k8s = self.k8s_statefulsets[i]
            if self.regex.match(k8s.metadata.name):
                statefulset = StatefulSet(k8s=k8s)

                if ((not k8s.status.ready_replicas)
                        or (k8s.status.ready_replicas < k8s.status.replicas)):
                    self.__logger.debug("a StatefulSet is in error")
                    self.failing_statefulsets.append(statefulset)
                    self.failing = True

                self.statefulsets.append(statefulset)

    def parse_daemonsets(self):
        """Parse the daemonsets."""
        self.__logger.info("%4s daemonsets to parse", len(self.k8s_daemonsets))
        for i in range(len(self.k8s_daemonsets)):
            k8s = self.k8s_daemonsets[i]
            if self.regex.match(k8s.metadata.name):
                daemonset = DaemonSet(k8s=k8s)

                if (k8s.status.number_ready <
                        k8s.status.desired_number_scheduled):
                    self.__logger.debug("a DaemonSet is in error")
                    self.failing_daemonsets.append(daemonset)
                    self.failing = True

                self.daemonsets.append(daemonset)

    def parse_pvcs(self):
        """Parse the persistent volume claims."""
        self.__logger.info("%4s pvcs to parse", len(self.k8s_pvcs))
        for i in range(len(self.k8s_pvcs)):
            k8s = self.k8s_pvcs[i]
            if self.regex.match(k8s.metadata.name):
                pvc = Pvc(k8s=k8s)

                if k8s.status.phase != "Bound":
                    self.__logger.debug("a PVC is in error")
                    self.failing_pvcs.append(pvc)
                    self.failing = True

                self.pvcs.append(pvc)

    def map_by_name(self, resources):
        return list(map(lambda resource: resource.name, resources))
